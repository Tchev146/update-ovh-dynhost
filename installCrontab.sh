#!/bin/bash

DATE=`date`
PWD=`pwd`


while getopts "l:p:h:" OPTION
do
    case $OPTION in

        l)
            LOGIN=$OPTARG
            ;;

        p)
            PASSWORD=$OPTARG
            ;;

        h)
            HOSTNAME=$OPTARG
            ;;

        *)
            echo "Incorrect options provided"
            exit 1
            ;;

    esac
done

if [[ "$LOGIN" == "" ]] || [[ "$PASSWORD" == "" ]]
then
    echo "[$DATE] You must provide an username and password -l and -p option"
    exit 2
fi

if [[ "$HOSTNAME" == "" ]]
then 
    echo "[$DATE] You must provide a hostname -h option"
    exit 3
fi

TASK="0 * * * * $PWD/updateOVHDynHost.sh -l $LOGIN -p $PASSWORD -h $HOSTNAME >> $PWD/updateOVHDynHost.log"

echo "[$DATE] add cron job : \"$TASK\""

CRON=`crontab -l 2>/dev/null`

(echo "$CRON"; echo "$TASK") | crontab -

exit 0
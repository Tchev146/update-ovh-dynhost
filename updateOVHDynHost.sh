#!/bin/bash

NEWIP=`wget -qO- ipinfo.io/ip`

DATE=`date`

while getopts "l:p:h:" OPTION
do
    case $OPTION in

        l)
            LOGIN=$OPTARG
            ;;

        p)
            PASSWORD=$OPTARG
            ;;

        h)
            HOSTNAME=$OPTARG
            ;;

        *)
            echo "Incorrect options provided"
            exit 1
            ;;

    esac
done

if [[ "$LOGIN" == "" ]] || [[ "$PASSWORD" == "" ]]
then
    echo "[$DATE] You must provide an username and password -l and -p option"
    exit 2
fi

if [[ "$HOSTNAME" == "" ]]
then 
    echo "[$DATE] You must provide a hostname -h option"
    exit 3
fi

if [[ -e ".publicIP_$HOSTNAME" ]]
then
	OLDIP=`cat .publicIP_$HOSTNAME`
else
	OLDIP=""
fi

echo "[$DATE] HOSTNAME : \"$HOSTNAME\""
echo "[$DATE] OLD IP : \"$OLDIP\", NEW IP : \"$NEWIP\""

REQ="http://www.ovh.com/nic/update?system=dyndns&hostname=$HOSTNAME&myip=$NEWIP"

if [[ "$OLDIP" == "" ]] || [[ ! "$OLDIP" == "$NEWIP" ]]
then 
    echo "[$DATE] curl --user \"$LOGIN:$PASSWORD\" $REQ"
    RETURN=`curl --silent --user "$LOGIN:$PASSWORD" $REQ`
    echo "[$DATE] curl response : $RETURN" 
    echo $NEWIP > .publicIP_$HOSTNAME
else 
    echo "[$DATE] No change needed !"
fi

exit 0
